# README #


### What is this repository for? ###

* This is a collection of reusable UI components for use with the AngularJs framework. It was developed and tested on Angular 1.6.  The Bootstrap 3.0 framework is required for styling.
* Version 1

### The collection consists of the following: ###
* app-wizard component - for creating a multi-step form for collecting information progressively. Component app-wiz-step is used in conjunction to define steps in the wizard.  See example below.
* wizard service -  for interacting with the wizard component
* nav-buttons component - Two button component providing conveneience methods for interfacing embedded forms with wizard service and component
* alert-dlg component - for use with AlertDlg service to create bootstrap modals (used with AlertDlg service)

### Features ###
#### Wizard ####
* Create declratively in markup, specifying the steps that make up the wizard
* Utilize service to manipulate wizard operation: Next, Previous, Goto
* Utilize service to change behaviour: disable/hide step at runtime, change step labelling
* May have multiple wizards per app
* May embed child wizard inside parent, to enable multiple sub-steps within a step
* Fully responsive, down to cell phone screen size

#### Nav Buttons ###
* Companion component to wizard
* Provide previous and next buttons with configuraion options
* May utilize promise mechanism to case asynch operation is required on Next (eg. sever lookup).  Until promise is resolved, next button is disabled
* May programtically disable next button
* May dynmaically change button text

## Sample usage: ##

### Wizard and Navbutton ###
```
<app-wizard wiz-id="main-wiz">
 
      <app-wiz-step id="PINFO" label="Personal Information">
	  	<form name="applFrm">
			...form fields
			<nav-buttons frm="applFrm" prev="$ctrl.Prev()" next="$ctrl.Next(form)"></nav-buttons>
		</form>
  	  </app-wiz-step>
	  
      <app-wiz-step id="BINFO" label="Business Information">
	  	<any-custom-component>
		</any-custom-component>
  	  </app-wiz-step>
	  
      <app-wiz-step id="STEPSPP" label="Steps within Step">
	  	<app-wizard wiz-id="child-wiz" parent-wiz-id="main-wiz">
      		<app-wiz-step id="CHILD1" label="Very long step - step 1">
	  		</app-wiz-step>
			<app-wiz-step id="CHILD2" label="Very long step - step 2">
	  		</app-wiz-step>
			<app-wiz-step id="CHILD3" label="Very long step - step 3">
	  		</app-wiz-step>
		</app-wizard>
  	  </app-wiz-step>
	  
	  <app-wiz-step id="REVIEW" label="Review & Submission">
  	  </app-wiz-step>
</app-wizard>
```

Inside host component's controller...
```
Controller.$inject = ['wizard'];
function Controller(wizard){
 var ctrl = this;
 ctrl.Next = function(form){
 	if(form.$invalid){
		//...notify user
	}else{
	 wizard.next('main-wiz');
	}
	
 }

 ctrl.Prev = function(){
	wizard.prev('main-wiz');
 }
}
```
### Alert dialog and service ###
Place alert-dlg tag at top of app component
'''<alert-dlg></alert-dlg>''' 

From any controller, inject AlertDlg service then call show
```
Controller.$inject = ['AlertDlg'];
function Controller(AlertDlg){
 //...
 AlertDlg.show("This is the dialog's header","This is the dialog's body text");
 //...
}

```
Todo: allow configurable id for dialog markup- currently hard coded to.  For now, id can be modified in source to overcome possible id clash with other element

## API ##
Check source code documentation for additional capabilities and usage 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact