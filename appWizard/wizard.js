'use strict';

/**
 * @ngdoc service
 * @name azWizard.wizard
 * @description
 * # wizard
 * Factory in the azWizard.
 */
angular.module('azWizard',[])
  .factory('wizard', function ($anchorScroll) {
  
  var _childWizardsCurrent = {};  //keep track of child wizard's current step id - needed for interaction with parent

  var _onSetStepStatus = {},
      _onPreviousStep = {},
      _onNextStep = {},
      _onGotoStep = {},
      _onGetActiveStepIndex = {},
      _onGetActiveStepId = {},
      _onGetNoSteps = {},
      _onUpdateStepLabel = {};

  var service={
    /**
     * * These public utility functions are used internally by the app-wizard component **
     */

    setChildCurrentStepId: function(wizId, stepId){
      _childWizardsCurrent[wizId] = stepId;
    },
    getChildCurrentStepId: function(wizId){
      return _childWizardsCurrent[wizId];
    },
    onSetStepStatus: function(wizId, fn){
      _onSetStepStatus[wizId] = fn;
    },
    onPreviousStep: function(wizId, fn){
      _onPreviousStep[wizId] = fn;
    },
    onNextStep: function(wizId, fn){
      _onNextStep[wizId] = fn;
    },
    onGotoStep: function(wizId, fn){
      _onGotoStep[wizId] = fn;
    },
    onGetActiveStepIndex: function(wizId, fn){
      _onGetActiveStepIndex[wizId] = fn;
    },
    onGetActiveStepId: function(wizId, fn){
      _onGetActiveStepId[wizId] = fn;
    },
    onGetNoSteps: function(wizId, fn){
      _onGetNoSteps[wizId] = fn;
    },
    onUpdateStepLabel: function(wizId, fn){
      _onUpdateStepLabel[wizId] = fn;
    },
    afterStepChanged:function(){
      $anchorScroll();
    },

    /**
     * These functions represent public API used by programmer 
     */

    /**
     * Returns the step id of the currently visible/active step
     * @param  {String} wizId id of wizard to query
     * @return {String}       Step id, as specified in markup
     */
    getActiveStepId: function(wizId){
      return _onGetActiveStepId[wizId] ? _onGetActiveStepId[wizId].call() : "";
    },
    /**
     * Advances wizard by one step
     * @param  {String}   wizId Identifies wizard instance to operate against
     * @return {Function}       [description]
     */
    next:function(wizId){
        _onNextStep[wizId].call(); 
        this.afterStepChanged();
    },
    /**
     * Moves wizard to previous step
     * @param  {String} wizId Identifies wizard instance to operate against
     * @return {[type]}       [description]
     */
    prev:function(wizId){
      _onPreviousStep[wizId].call();
      this.afterStepChanged();
    },
    /**
     * Retrives internal index of active step (1 for first, 2 for second...etc)
     * @param  {String} wizId Identifies wizard instance to operate against
     * @return {Number}       Step index for current active/visible step; -1 if no active step is active (likely error)
     */
    getActiveStepIndex:function(wizId){
      return _onGetActiveStepIndex[wizId] ? _onGetActiveStepIndex[wizId].call() + 1 : -1;
    },
    /**
     * Makes a step become active, without the restrictions of prev() and next() functions
     * @param  {String} wizId Identifies wizard instance to operate against
     * @param  {String} stepId Step id of step to make active
     * @return {[type]}        [description]
     */
    gotoStep:function(wizId, stepId){
      _onGotoStep[wizId].call(null,stepId);
      this.afterStepChanged();
    },
    /**
     * Updates specified step, making it either enabled or disabled.
     * Note that disabled steps are invisible and do not participate in navigation.
     * @param {String} wizId Identifies wizard instance to operate against
     * @param {String} stepId Step to enable/disable
     * @param {Boolean} status Specifies new status. true: enables step; false: disables step
     */
    setStepStatus: function(wizId, stepId, status){
      _onSetStepStatus[wizId].call(null,stepId, status);
    },
    /**
     * Returns the number of steps currently enabled on the wizard (only enabled steps can be navigated to).
     * This number changes after setStepStatus() modifies the wizard.
     * @param  {String} wizId Identifies wizard instance to operate against
     * @return {Number}       Number of steps
     */
    getNoEnabledSteps: function(wizId){
      return _onGetNoSteps[wizId] ? _onGetNoSteps[wizId].call() : 0; 
    },
    /**
     * Changes a step's label
     * @param  {String} wizId Identifies wizard instance to operate against
     * @param  {String} stepId   Id of step to rename
     * @param  {String} newLabel Text representing new label for step
     * @return {[type]}          [description]
     */
    updateStepLabel:function(wizId, stepId, newLabel){
      _onUpdateStepLabel[wizId].call(null,stepId,newLabel);
    }
  };
  return service;
});
