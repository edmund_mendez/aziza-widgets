'use strict';

/**
 * @ngdoc component
 * @name azWizard.component:appWizStep
 * @description
 * # appWizStep
 */
angular.module('azWizard')
  .component('appWizStep', {
  	bindings:{
  		id:'@', //step id
  		label:'@',  //step label
  		isDisabled:'@'  //is step disabled/enabled
  	},
  	require:{
  		appWizard:'^^'
  	},
  	controller: AppWizController,
  	templateUrl:'appwizstep.html',
  	transclude:true
  });

function AppWizController(){
	var ctrl=this;

	ctrl.$onInit = function(){
		ctrl.stepState = {
			id: ctrl.id,
			label:ctrl.label,
			isDisabled: !!ctrl.isDisabled,	//to hide step from wizard activity (next/prev will jump over it etc)
			isCurrent: false	//by default don't show step
		};
		ctrl.appWizard.addStep(ctrl.stepState);

	};

}