'use strict';

/**
 * @ngdoc component
 * @name azWizard.component:appWizard
 * @description
 * # appWizard
 */
angular.module('azWizard')
  .component('appWizard', {
    controller: AppWizard,
    transclude:true,
    templateUrl: 'appwizard.html',
    bindings:{
        wizId:'@',  //mandatory wizard id that uniquely identifies each wizard instance
        parentWizId:'@' //wizard id of parent wizard. Used only if wizard is embedded into a parent wizard's step
    }
  });

AppWizard.$inject=['wizard']
function AppWizard(wizard){
	var ctrl=this;
    ctrl.steps = [];    //all the declared steps
    ctrl.enabledSteps = []; //subset of declared: only those that are not disabled
    ctrl.currentStep = null;    //object containing the properties of the curent step..id, label etc
    ctrl.stepsById = {};    //data structure to quickly retrieve step object from provision of id

    ctrl.$postLink = function(){
        if(ctrl.parentWizId){   //if child wizard, check if there is a cached step state
            var cachedStep = wizard.getChildCurrentStepId(ctrl.wizId);
            if(cachedStep){
                wizard.go(ctrl.wizId,cachedStep); //if it is, on reveal go to step that was active before handing off to parent wizard
            }
        }
    };

	ctrl.$onInit = function(){

        wizard.onSetStepStatus(ctrl.wizId,function(stepId, status){  //invoked when a step's status is enabled/disabled
            ctrl.stepsById[stepId].isDisabled = !status;
            ctrl.enabledSteps = [];
            ctrl.steps.forEach(function(step){
                if(!step.isDisabled){
                    ctrl.enabledSteps.push(step);
                    step.idx = ctrl.enabledSteps.length-1;
                }
            });
        });

        wizard.onPreviousStep(ctrl.wizId,function(){    //invoked when wizard.prev(id) is called
            if(ctrl.enabledSteps[0].isCurrent){ //at begining of wizard
                if(ctrl.parentWizId){   //this is an embedded wizard, call previous on parent
                    wizard.setChildCurrentStepId(ctrl.wizId,ctrl.currentStep.id);   //we'll resume at this step 
                    wizard.prev(ctrl.parentWizId);
                    return;
                }else{  //otherwise can't go to previous step
                    return;
                }
            }
            var newIndex = ctrl._getCurrentIdx() - 1;
            ctrl.currentStep.isCurrent = false;
            ctrl.currentStep = ctrl.enabledSteps[newIndex];
            ctrl.currentStep.isCurrent = true;
        });

        wizard.onNextStep(ctrl.wizId,function(){    //invoked when wizard.next(id) is called
            if(ctrl.enabledSteps[ctrl.enabledSteps.length-1].isCurrent){    //already at the end, can't go next
                if(ctrl.parentWizId){   //this is an embedded wizard, call next on parent
                    wizard.setChildCurrentStepId(ctrl.wizId,ctrl.currentStep.id); //we'll resume at this step 
                    wizard.next(ctrl.parentWizId);
                    return;
                }else{  //otherwise can't go to previous step
                    return;
                }
            }
            var newIndex = ctrl._getCurrentIdx()+1;
            ctrl.currentStep.isCurrent = false;
            ctrl.currentStep = ctrl.enabledSteps[newIndex];
            ctrl.currentStep.isCurrent = true; 
        });

        wizard.onGotoStep(ctrl.wizId,function(stepId){  //invoked when wizard.go(wizid, newstepid) is called
            ctrl.currentStep.isCurrent = false;
            ctrl.currentStep = ctrl.stepsById[stepId];
            ctrl.currentStep.isCurrent = true;
        });

        wizard.onGetActiveStepIndex(ctrl.wizId,function(){  //wizard.getActiveStepIndex()
            return ctrl.currentStep.idx;
        });
        wizard.onGetActiveStepId(ctrl.wizId,function(){ //wizard.getActiveStepId(wizid)
            return ctrl.currentStep.id;
        });
        wizard.onGetNoSteps(ctrl.wizId,function(){
            return ctrl.enabledSteps.length;
        });
        
        wizard.onUpdateStepLabel(ctrl.wizId,function(stepId,newLabel){
            ctrl.stepsById[stepId].label = newLabel;
        });

	}

    ctrl._getCurrentIdx = function(){
        return ctrl.currentStep.idx;
    }
    /**
     * Function called by wiz-step child component for each wizard step
     * @param {Object} step State of each step passed by wiz-step component
     */
    ctrl.addStep = function(step){
        ctrl.steps.push(step);  //add to general list of steps
        if(!step.isDisabled){   
            ctrl.enabledSteps.push(step);   //...to enabled steps (these can be navigated to)
            step.idx = ctrl.enabledSteps.length-1;  //decorate object with step's index (prevent traversing array to get info)
        }
        if(!ctrl.steps[0].isCurrent){   //if a current step (visible) is not set, set to the first step
            ctrl.steps[0].isCurrent=true;
            ctrl.currentStep = step;
        }
        ctrl.stepsById[step.id] = step;
    }

	ctrl.getStep = function(){
        return ctrl.currentStep ? ctrl.currentStep.idx : 0;
    };

    /**
     * To allow clickiong on step label on wizard widget to jump to a previous step
     * @param {String} stepId Step id of step to navigate to
     */
    ctrl.stepGo = function(stepId){
        if(ctrl.stepsById[stepId].idx > ctrl.currentStep.idx){return;}  //ensure navigating to previous step only 
        wizard.gotoStep(ctrl.wizId,stepId);
    };
    
} 