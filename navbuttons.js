'use strict';

/**
 * @ngdoc component
 * @name azNavButtons.component:navButtons
 * @description
 * # navButtons
 */

angular.module('azNavButtons',[]).
component('navButtons',{
  controller: NbController,
  template: [ '<button class="btn btn-primary" ng-hide="$ctrl.hideButton" ng-click="$ctrl.prev()">',
              '<span class="glyphicon glyphicon-circle-arrow-left"></span> PREVIOUS',
              '</button>',
              ' <button class="btn" ng-disabled="$ctrl.doDisable()" ng-class="$ctrl.getClass()" ng-click="$ctrl.onClick($event)">{{$ctrl.buttonText}} ',
              '<span class="glyphicon glyphicon-circle-arrow-right"></span>',
              '</button>'].join(''),
  bindings:{
        frm:'=',  //Mandatory.Form name that hosts component
        prev:'&prev', //Mandatory. Controller function to call when previous button is clicked
        next:'&next', //Mandatory. Controller functoin to call when next button is clicked
        hidePrev:'@', //Optional. Indicate whether previous button should be hidded (eg. on the first step)
        nextSubmits:'@',  //Optional. Indicates that next button will submit rather than go next (formatting purpose: color, text)
        /*Optional.  /When true, disableOnNext disables the next button when clicked. 
          This is needed for asynch operations. 
          Note: The called function, next(), must return a promise when this field is true
          When the promise resolved, the field is re-enabled
        */
        disableOnNext:'@',  
        disableNext:'=' // optional: disables next button when passed expression is true   
  }
});

function NbController(){
  var ctrl = this;
  
  ctrl.onClick = function(event){
    if(ctrl.disableOnNext){
      event.target.disabled = true;
      ctrl.next({form:ctrl.frm}).finally(function(){
        event.target.disabled = false;
      });
    }else{
      ctrl.next({form:ctrl.frm})
    }
  }
  
  ctrl.doDisable = function(){
    return ctrl.disableNext ? ctrl.disableNext : false;
  }

  ctrl.$onInit = function(){
    ctrl.hideButton = ctrl.hidePrev=="";
    ctrl.buttonText = ctrl.nextSubmits || "NEXT";
  };

  ctrl.getClass = function(){
    return {
      'btn-primary' : !ctrl.nextSubmits,
      'btn-danger' : ctrl.nextSubmits
    };
  }
}
