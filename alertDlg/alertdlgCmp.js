'use strict';

/**
 * @ngdoc component
 * @name azBsDlg.component:alertDlg
 * @description
 * # alertDlg
 */
angular.module('azBsDlg',[])
.factory('AlertDlg', function () {
    // Service logic
    var observer = null, context=null;

    // Public API here
    return {
      show: function (header,body) {
        observer.call(context,header,body);
      },
      onShow: function(fnc,ctx){
        observer = fnc;
        context = ctx;
      }

    };
})
.component('alertDlg', {
  	controller: AlertDlgComp,
  	templateUrl: 'alertdlgCmp.html',
  	replace: true
});

AlertDlgComp.$inject = ['AlertDlg'];
function AlertDlgComp(AlertDlg){

	var ctrl = this;
	
	ctrl.$onInit = function(){
		AlertDlg.onShow(function(header, body){
			ctrl.header = header;
			ctrl.body = body;
			$('#alert-dlg').modal();
		},ctrl);
	};
}